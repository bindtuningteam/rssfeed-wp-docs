![Layout options.PNG](../images/classic/12.layouts.png)

----
### Layout Template

Lets you select from a collection of stylings for presenting each item: 

**Basic**
![Basic layout.PNG](../images/classic/13.basiclayout.png)

**Basic Card**
![Basic Card Layout.PNG](../images/classic/14.cardlayout.png)

**Gradient**
![Gradient Layout.PNG](../images/classic/15.gradientlayout.png)

**Gradient Wide**
![Gradient Wide Layout.PNG](../images/classic/16.gradientwidelayout.png)

**Boxed**
![Boxed Layout.PNG](../images/classic/17.boxedlayout.png)

**Condensed**
![Condensed Layout.PNG](../images/classic/18.condensedlayout.png)


----
### Number of Columns

Lets you select the maximum number of columns that can be created, up to 4.
The web part will only render the set number of columns if there's enough space to allow for it. Otherwise, the number of columns will be automatically adjusted for the user's screen size.

----
### Display Limit

Lets you limit the amount of items that get rendered on the page. By default, the value is 5.

