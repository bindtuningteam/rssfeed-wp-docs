![Item Sorting Options.PNG](../images/classic/22.orderby.png)

----
### Order By

Lets you decide how items should be sorted. 
You can choose a field from the dropdown and select the sort order from Ascending or Descending.

For tie breaking, its possible to define more than one sorting option by clicking on **Add Order by**.