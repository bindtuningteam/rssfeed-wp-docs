![Item Sorting Options.PNG](../images/classic/23.advanced.png)

----
### Language

From here you can select which language you want to be displayed by the web part. This will also translate the forms, but a refresh may be required before you see the changes.

If you'd like to localize the web part to your own language, please refer to this article.