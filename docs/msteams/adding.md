<p class="alert alert-info">Solution for Microsoft Teams is available since RSS Reader Web Part version 1.1.5.14.</p>

1. Open the Team on **Teams panel** that you intend to add the Web Part; 
2. Click on the **[+]** button to add a new **Tab** to the Team;
3. On the Search type **RSS Reader** and select the **BindTuning RSS Reader**
4. It'll open a modal where you can post that you've added a new tab or not. After selecting either or not to post, click on **Save**. 
5. The tab will be added to the team. 

Now the only thing left to do is to connect the web part and setup the rest of the **[Web Part Properties](./general.md)**. 

![add_tab_teams.gif](../images/msteams/add_tab_teams.gif)

