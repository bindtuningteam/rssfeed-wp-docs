1. On the Tab click on the dropdown and then **Settings**. If is the first time adding the **RSS Reader**, you can skip this process. 

    ![settings_delete.png](../images/msteams/setting_edit.png)


6. Configure the web part according to the settings described in the **[Web Part Properties](./general.md)**;

    ![06.options.png](../images/modern/06.options.png)

3. The properties are saved automatically, so when you're done click to close the Properties. 

