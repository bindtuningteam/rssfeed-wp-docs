![Item Sorting Options.PNG](../images/modern/11.contentoptions.png)

----
### Click Action

Decide what you want to happen when you click on an article
- **Show Article Content** - Shows the content in a modal with a preformated layout based on the mappings of the feed. 
- **Open Article Site** - Will load the page mapped to the "Url" field in the feed options. How this page is loaded depends on the "Open Site In" option below

### Open Site In
If the "Click Action" is set to **Open Article Site**, this option lets you choose how exactly you'd like for it to open

- **New Window** - will launch a new browser window or tab with the target page
- **Same Window** - will navigate to the target page and leave the current SharePoint page
- **Modal** - will try to launch the target page inside an iframe. If the url for the page is not using `https://` or blocking external domains, the content may not show.