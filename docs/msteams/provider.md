![Layout options.PNG](../images/classic/04.provideroptions.png)

___
### Provider API Key

The web part used the [rss2json](http://rss2json.com) API to retrieve items from RSS or ATOM feeds. Before the web part can work, you must paste your API key into this field.

To create your API Key, follow the steps below:

1. Navigate to the <a href="https://rss2json.com/plans" target="_blank">plans page</a> on the service's site
2. Select your plan and create a new account
3. Navigate to the [API Key](https://rss2json.com/me/api_key) page inside your new account page
4. Copy the key on this page and paste it into the form

<p class="alert alert-info">
Keep in mind there's a limit to how many feeds you can associate to your key. 
These feeds will be automatically registered as you request them from the API. 
If you reach the limit, you can manage the registered feeds from <a href="https://rss2json.com/me/feed" target="_blank">your account page.</a></p>
