### Feed Options

<p class="alert alert-info">Note that this section is only visible once you set API Key in the <a href="../provider">Provider Options</a> section.
</p>

![Layout options.PNG](../images/classic/05.feedoptions.png)

**Add Feed**

For your web part to work, you need to connect it to a RSS Feed. To do so, click on Add Feed and paste the url to your desired feed.

![Field Options](../images/classic/19.feed-add.gif)


**Field Mapping Options**

Under Field Mapping Options, you will see a number of dropdowns. These dropdowns include all the fields available in the feed. From here you can associate these fields to the existing fields of the web part

![Field Mapping Options](../images/classic/20.feed-mapings.png)

**Feed Meta Data** 


Under Feed Meta Data, you can select some extra display options for your items.

**Label** - will be used to identify the feed and will show in each item from the feed

**Default Image** - option url to an image that gets displayed if an article doesn't have an image mapped. Recommended to use the logo for the feed source

**Color** - optional color to help identify the feed source in some templates

**Icon** - optional icon to help identify the feed source in some templates

![Feed Metadata Options](../images/classic/21.feed-metadata.png)

After setting all the feed options, don't forget to click on the save icon to save them. You can always add more than one list by clicking again on **Add Feed**.

![Feed Metadata Options](../images/classic/25.mappings-card.png)
![Feed Metadata Options](../images/classic/26.mappings-content.png)

----
### Order By

Lets you decide how items should be sorted. 
You can choose a field from the dropdown and select the sort order from Ascending or Descending.

![Item Sorting Options.PNG](../images/classic/22.orderby.png)

For tie breaking, its possible to define more than one sorting option by clicking on **Add Order by**.

---
![Feed Options](../images/modern/03.provideroptions.gif)